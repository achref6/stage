package stage.repository;

import org.springframework.data.repository.CrudRepository;

import stage.model.Mesure;

public interface MesureRepository extends CrudRepository<Mesure, Integer> {

}
