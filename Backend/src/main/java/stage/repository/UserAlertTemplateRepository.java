package stage.repository;

import org.springframework.data.repository.CrudRepository;

import stage.model.UserAlertTemplate;

public interface UserAlertTemplateRepository extends CrudRepository<UserAlertTemplate, Integer> {

}
