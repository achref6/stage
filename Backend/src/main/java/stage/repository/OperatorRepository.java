package stage.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import stage.model.Operator;

public interface OperatorRepository extends CrudRepository<Operator, Integer> {

	List<Operator> findByMesuresId(int id);

}
