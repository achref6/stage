package stage.repository;

import org.springframework.data.repository.CrudRepository;

import stage.model.UserAlert;

public interface UserAlertRepository extends CrudRepository<UserAlert, Integer> {

}
