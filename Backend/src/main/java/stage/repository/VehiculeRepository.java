package stage.repository;

import org.springframework.data.repository.CrudRepository;

import stage.model.Vehicule;

public interface VehiculeRepository extends CrudRepository<Vehicule, Long> {

}
