package stage.repository;

import org.springframework.data.repository.CrudRepository;

import stage.model.AlertConfiguration;

public interface AlertConfigurationRepository extends CrudRepository<AlertConfiguration, Integer> {

}
