package stage.repository;

import org.springframework.data.repository.CrudRepository;

import stage.model.Groupe;

public interface GroupeRepository extends CrudRepository<Groupe, Long> {

}
