package stage.repository;

import org.springframework.data.repository.CrudRepository;

import stage.model.AlertConfigurationTemplate;

public interface AlertConfigurationTemplateRepository extends CrudRepository<AlertConfigurationTemplate, Integer> {

}
