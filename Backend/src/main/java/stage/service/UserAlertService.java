package stage.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stage.model.AlertConfiguration;
import stage.model.UserAlert;
import stage.repository.UserAlertRepository;

@Service
public class UserAlertService {

	@Autowired
	private UserAlertRepository userAlertRepository;
	@Autowired
	private AlertConfigurationService alertConfigurationService;

	
	public List<UserAlert> getAllUserAlerts() {
		List<UserAlert> userAlerts = new ArrayList<UserAlert>();
		userAlertRepository.findAll().forEach(userAlerts::add);
		return userAlerts;
	}
	
	public UserAlert getUserAlert(int id){
		return userAlertRepository.findById(id).get();
	}
	
	public void addUserAlert(UserAlert userAlert) {
		Set<AlertConfiguration> configs=userAlert.getAlertconfigurations();
		userAlert.setAlertconfigurations(null);
		UserAlert userAlertResponse=userAlertRepository.save(userAlert);
		for(AlertConfiguration config : configs) {
			config.setUserAlert(userAlertResponse);
			alertConfigurationService.addAlertConfiguration(config);
		}
		
	}
	
	public void updateUserAlert(UserAlert userAlert) {
		Set<AlertConfiguration> configs=userAlert.getAlertconfigurations();
		userAlert.setAlertconfigurations(null);
		UserAlert userAlertResponse=userAlertRepository.save(userAlert);
		for(AlertConfiguration config : configs) {
			config.setUserAlert(userAlertResponse);
			alertConfigurationService.addAlertConfiguration(config);
		}
	}
	
	public void deleteUserAlert(int id) {
		userAlertRepository.deleteById(id);
	}

}
