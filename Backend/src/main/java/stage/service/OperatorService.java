package stage.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stage.model.Operator;
import stage.repository.OperatorRepository;

@Service
public class OperatorService {

	@Autowired
	private OperatorRepository operatorRepository;

	public List<Operator> getAllOperators() {
		List<Operator> operators = new ArrayList<Operator>();
		operatorRepository.findAll().forEach(operators::add);
		return operators;
	}

	public Operator getOperator(int id){
		return operatorRepository.findById(id).get();
	}

	public void addOperator(Operator operator) {
		operatorRepository.save(operator);
	}

	public void updateOperator(Operator operator) {
		operatorRepository.save(operator);
	}

	public void deleteOperator(int id) {
		operatorRepository.deleteById(id);
	}

	public List<Operator> getOperatorByMesureId(int id) {
		return operatorRepository.findByMesuresId(id);
	}

}
