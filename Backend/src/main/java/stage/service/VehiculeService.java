package stage.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stage.model.Vehicule;
import stage.repository.VehiculeRepository;

@Service
public class VehiculeService {
	@Autowired
	private VehiculeRepository vehiculeRepository;
	
	public List<Vehicule> getAllVehicules() {
		List<Vehicule> vehicules = new ArrayList<Vehicule>();
		vehiculeRepository.findAll().forEach(vehicules::add);
		return vehicules;
	}



}
