package stage.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stage.model.AlertConfiguration;
import stage.repository.AlertConfigurationRepository;

@Service
public class AlertConfigurationService {

	@Autowired
	private AlertConfigurationRepository alertConfigurationRepository;

	public List<AlertConfiguration> getAllAlertConfigurations() {
		List<AlertConfiguration> alertConfigurations = new ArrayList<AlertConfiguration>();
		alertConfigurationRepository.findAll().forEach(alertConfigurations::add);
		return alertConfigurations;
	}

	public AlertConfiguration getAlertConfiguration(int id){
		return alertConfigurationRepository.findById(id).get();
	}

	public void addAlertConfiguration(AlertConfiguration alertConfiguration) {
		alertConfigurationRepository.save(alertConfiguration);
	}

	public void updateAlertConfiguration(AlertConfiguration alertConfiguration) {
		alertConfigurationRepository.save(alertConfiguration);
	}

	public void deleteAlertConfiguration(int id) {
		alertConfigurationRepository.deleteById(id);
	}

}
