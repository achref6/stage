package stage.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stage.model.AlertConfigurationTemplate;
import stage.repository.AlertConfigurationTemplateRepository;

@Service
public class AlertConfigurationTemplateService {

	@Autowired
	private AlertConfigurationTemplateRepository alertConfigurationTemplateRepository;

	public List<AlertConfigurationTemplate> getAllAlertConfigurationTemplates() {
		List<AlertConfigurationTemplate> alertConfigurationTemplates = new ArrayList<AlertConfigurationTemplate>();
		alertConfigurationTemplateRepository.findAll().forEach(alertConfigurationTemplates::add);
		return alertConfigurationTemplates;
	}

	public AlertConfigurationTemplate getAlertConfigurationTemplate(int id){
		return alertConfigurationTemplateRepository.findById(id).get();
	}

	public void addAlertConfigurationTemplate(AlertConfigurationTemplate alertConfigurationTemplate) {
		alertConfigurationTemplateRepository.save(alertConfigurationTemplate);
	}

	public void updateAlertConfigurationTemplate(AlertConfigurationTemplate alertConfigurationTemplate) {
		alertConfigurationTemplateRepository.save(alertConfigurationTemplate);
	}

	public void deleteAlertConfigurationTemplate(int id) {
		alertConfigurationTemplateRepository.deleteById(id);
	}

}
