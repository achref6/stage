package stage.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stage.model.Mesure;
import stage.repository.MesureRepository;

@Service
public class MesureService {

	@Autowired
	private MesureRepository mesureRepository;

	public List<Mesure> getAllMesures() {
		List<Mesure> mesures = new ArrayList<Mesure>();
		mesureRepository.findAll().forEach(mesures::add);
		return mesures;
	}

	public Mesure getMesure(int id){
		return mesureRepository.findById(id).get();
	}

	public void addMesure(Mesure mesure) {
		mesureRepository.save(mesure);
	}

	public void updateMesure(Mesure mesure) {
		mesureRepository.save(mesure);
	}

	public void deleteMesure(int id) {
		mesureRepository.deleteById(id);
	}

}
