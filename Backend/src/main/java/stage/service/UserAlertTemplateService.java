package stage.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stage.model.AlertConfiguration;
import stage.model.AlertConfigurationTemplate;
import stage.model.UserAlert;
import stage.model.UserAlertTemplate;
import stage.repository.UserAlertTemplateRepository;

@Service
public class UserAlertTemplateService {

	@Autowired
	private UserAlertTemplateRepository userAlertTemplateRepository;
	@Autowired
	private AlertConfigurationTemplateService alertConfigurationTemplateService;


	public List<UserAlertTemplate> getAllUserAlertTemplates() {
		List<UserAlertTemplate> userAlertTemplates = new ArrayList<UserAlertTemplate>();
		userAlertTemplateRepository.findAll().forEach(userAlertTemplates::add);
		return userAlertTemplates;
	}

	public UserAlertTemplate getUserAlertTemplate(int id){
		return userAlertTemplateRepository.findById(id).get();
	}

	public void addUserAlertTemplate(UserAlertTemplate userAlertTemplate) {
		Set<AlertConfigurationTemplate> configs=userAlertTemplate.getAlertconfigurations();
		userAlertTemplate.setAlertconfigurations(null);
		UserAlertTemplate userAlertResponse=userAlertTemplateRepository.save(userAlertTemplate);
		for(AlertConfigurationTemplate config : configs) {
			config.setUserAlert(userAlertResponse);
			alertConfigurationTemplateService.addAlertConfigurationTemplate(config);
		}

	}

	public void updateUserAlertTemplate(UserAlertTemplate userAlertTemplate) {
		Set<AlertConfigurationTemplate> configs=userAlertTemplate.getAlertconfigurations();
		userAlertTemplate.setAlertconfigurations(null);
		UserAlertTemplate userAlertResponse=userAlertTemplateRepository.save(userAlertTemplate);
		for(AlertConfigurationTemplate config : configs) {
			config.setUserAlert(userAlertResponse);
			alertConfigurationTemplateService.addAlertConfigurationTemplate(config);
		}
	}

	public void deleteUserAlertTemplate(int id) {
		userAlertTemplateRepository.deleteById(id);
	}

}
