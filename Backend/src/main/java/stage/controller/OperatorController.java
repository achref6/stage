package stage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import stage.model.Operator;
import stage.service.OperatorService;
@CrossOrigin

@RestController
@RequestMapping("/operator")
public class OperatorController {

	@Autowired
	private OperatorService operatorService;

	@GetMapping
	public List<Operator> getAllOperator() {
		return operatorService.getAllOperators();
	}

	@GetMapping("/{id}")
	public Operator getOperator(@PathVariable int id){
		return operatorService.getOperator(id);
	}
	
	@GetMapping("/mesure/{id}")
	public List<Operator> getOperatorByMesureId(@PathVariable int id){
		return operatorService.getOperatorByMesureId(id);
	}


	@PostMapping
	public void addOperator(@RequestBody Operator operator) {
		operatorService.addOperator(operator);
	}

	@PutMapping
	public void updateOperator(@RequestBody Operator operator) {
		operatorService.updateOperator(operator);
	}

	@DeleteMapping("/{id}")
	public void deleteOperator(@PathVariable int id) {
		operatorService.deleteOperator(id);
	}

}
