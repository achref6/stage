package stage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import stage.model.Vehicule;
import stage.service.VehiculeService;

@CrossOrigin

@RestController
@RequestMapping("/vehicule")
public class VehiculeController {
	@Autowired
	private VehiculeService vehiculeService;

	@GetMapping
	public List<Vehicule> getAllOperator() {
		return vehiculeService.getAllVehicules();
	}

}
