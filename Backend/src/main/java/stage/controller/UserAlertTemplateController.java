package stage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import stage.model.UserAlertTemplate;
import stage.service.UserAlertTemplateService;
@CrossOrigin

@RestController
@RequestMapping("/useralerttemplate")
public class UserAlertTemplateController {

	@Autowired
	private UserAlertTemplateService userAlertTemplateService;

	@GetMapping
	public List<UserAlertTemplate> getAllUserAlertTemplate() {
		return userAlertTemplateService.getAllUserAlertTemplates();
	}

	@GetMapping("/{id}")
	public UserAlertTemplate getUserAlertTemplate(@PathVariable int id){
		return userAlertTemplateService.getUserAlertTemplate(id);
	}

	@PostMapping
	public void addUserAlertTemplate(@RequestBody UserAlertTemplate userAlertTemplate) {
		userAlertTemplateService.addUserAlertTemplate(userAlertTemplate);
	}

	@PutMapping
	public void updateUserAlertTemplate(@RequestBody UserAlertTemplate userAlertTemplate) {
		userAlertTemplateService.updateUserAlertTemplate(userAlertTemplate);
	}

	@DeleteMapping("/{id}")
	public void deleteUserAlertTemplate(@PathVariable int id) {
		userAlertTemplateService.deleteUserAlertTemplate(id);
	}

}
