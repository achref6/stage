package stage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import stage.model.Mesure;
import stage.service.MesureService;
@CrossOrigin

@RestController
@RequestMapping("/mesure")
public class MesureController {

	@Autowired
	private MesureService mesureService;

	@GetMapping
	public List<Mesure> getAllMesure() {
		return mesureService.getAllMesures();
	}

	@GetMapping("/{id}")
	public Mesure getMesure(@PathVariable int id){
		return mesureService.getMesure(id);
	}

	@PostMapping
	public void addMesure(@RequestBody Mesure mesure) {
		mesureService.addMesure(mesure);
	}

	@PutMapping
	public void updateMesure(@RequestBody Mesure mesure) {
		mesureService.updateMesure(mesure);
	}

	@DeleteMapping("/{id}")
	public void deleteMesure(@PathVariable int id) {
		mesureService.deleteMesure(id);
	}

}
