package stage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import stage.model.UserAlert;
import stage.service.UserAlertService;
@CrossOrigin

@RestController
@RequestMapping("/useralert")
public class UserAlertController {
	@Autowired
	private UserAlertService userAlertService;

	@GetMapping
	public List<UserAlert> getAllUserAlert() {
		return userAlertService.getAllUserAlerts();
	}

	@GetMapping("/{id}")
	public UserAlert getUserAlert(@PathVariable int id){
		return userAlertService.getUserAlert(id);
	}

	@PostMapping
	public void addUserAlert(@RequestBody UserAlert userAlert) {
		userAlertService.addUserAlert(userAlert);
	}

	@PutMapping
	public void updateUserAlert(@RequestBody UserAlert userAlert) {
		userAlertService.updateUserAlert(userAlert);
	}

	@DeleteMapping("/{id}")
	public void deleteUserAlert(@PathVariable int id) {
		userAlertService.deleteUserAlert(id);
	}

}
