package stage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import stage.model.AlertConfiguration;
import stage.model.UserAlert;
import stage.service.AlertConfigurationService;
@CrossOrigin

@RestController
@RequestMapping("/alertconfiguration")
public class AlertConfigurationController {

	@Autowired
	private AlertConfigurationService alertConfigurationService;

	@GetMapping
	public List<AlertConfiguration> getAllAlertConfiguration() {
		return alertConfigurationService.getAllAlertConfigurations();
	}

	@GetMapping("/{id}")
	public AlertConfiguration getAlertConfiguration(@PathVariable int id){
		return alertConfigurationService.getAlertConfiguration(id);
	}

	@PostMapping
	public void addAlertConfiguration(@RequestBody AlertConfiguration alertConfiguration) {
		alertConfigurationService.addAlertConfiguration(alertConfiguration);
	}

	@PutMapping
	public void updateAlertConfiguration(@RequestBody AlertConfiguration alertConfiguration) {
		alertConfigurationService.updateAlertConfiguration(alertConfiguration);
	}

	@DeleteMapping("/{id}")
	public void deleteAlertConfiguration(@PathVariable int id) {
		alertConfigurationService.deleteAlertConfiguration(id);
	}

}
