package stage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import stage.model.AlertConfigurationTemplate;
import stage.service.AlertConfigurationTemplateService;
@CrossOrigin

@RestController
@RequestMapping("/alertconfigurationtemplate")
public class AlertConfigurationTemplateController {

	@Autowired
	private AlertConfigurationTemplateService alertConfigurationTemplateService;

	@GetMapping
	public List<AlertConfigurationTemplate> getAllAlertConfigurationTemplate() {
		return alertConfigurationTemplateService.getAllAlertConfigurationTemplates();
	}

	@GetMapping("/{id}")
	public AlertConfigurationTemplate getAlertConfigurationTemplate(@PathVariable int id){
		return alertConfigurationTemplateService.getAlertConfigurationTemplate(id);
	}

	@PostMapping
	public void addAlertConfigurationTemplate(@RequestBody AlertConfigurationTemplate alertConfigurationTemplate) {
		alertConfigurationTemplateService.addAlertConfigurationTemplate(alertConfigurationTemplate);
	}

	@PutMapping
	public void updateAlertConfigurationTemplate(@RequestBody AlertConfigurationTemplate alertConfigurationTemplate) {
		alertConfigurationTemplateService.updateAlertConfigurationTemplate(alertConfigurationTemplate);
	}

	@DeleteMapping("/{id}")
	public void deleteAlertConfigurationTemplate(@PathVariable int id) {
		alertConfigurationTemplateService.deleteAlertConfigurationTemplate(id);
	}

}
