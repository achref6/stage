package stage.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "alertconfigurationoperator")
public class Operator {
	@Id
	@Column(name = "id")
	private int id;
	
	@Column(name = "name")
	private String name;
	@JsonBackReference
	@ManyToMany(mappedBy = "operators")
	private Set<Mesure> mesures;
}
