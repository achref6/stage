package stage.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "useralert")
public class UserAlert extends AbstractUserAlert {
	@ManyToOne
	@JoinColumn(name = "device_id")
	private Vehicule vehicule;
	
	@ManyToOne
	@JoinColumn(name = "group_id")
	private Groupe groupe;
	
	@Cascade(CascadeType.ALL)
	@ManyToMany(mappedBy = "userAlert")
	private Set<AlertConfiguration> alertconfigurations;


}
