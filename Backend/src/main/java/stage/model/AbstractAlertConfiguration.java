package stage.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractAlertConfiguration {
	@Id
	@Column(name = "id")
	@GeneratedValue
	protected int id;
	
	@ManyToOne
	@JoinColumn(name = "operator_id")
	protected Operator operator;
	
	@Column(name = "value1")
	protected String value1;
	
	@Column(name = "value2")
	protected String value2;
	@ManyToOne
	@JoinColumn(name = "mesure_id")
	protected Mesure mesure;
	
	@Column(name = "is_active")
	protected boolean active;

}
