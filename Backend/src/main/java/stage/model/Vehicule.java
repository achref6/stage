package stage.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "vehicule")
public class Vehicule {
	@Id
	@Column(name = "vehicule_id")
	private long id;
	@Column(name="alias")
	private String alias;
	@ManyToMany
	@JoinTable(
	  name = "vehicule_groupe", 
	  joinColumns = @JoinColumn(name = "vehicule_id"), 
	  inverseJoinColumns = @JoinColumn(name = "groupe_id"))
	private Set<Groupe> groupes;



}
