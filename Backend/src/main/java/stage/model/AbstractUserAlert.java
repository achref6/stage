package stage.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractUserAlert {

	@Id
	@Column(name = "id")
	@GeneratedValue
	protected int id;
	
	@Column(name = "name")
	protected String name;
	
	@Column(name = "type")
	protected String frequency;
	
	@Column(name = "time_to_realert")
	protected int timeToRealert;
	
	@Column(name = "is_active")
	protected boolean active;
	
	@Column(name = "email_mode")
	protected boolean emailMode;
	
	@Column(name = "emails")
	protected String email;
}
