package stage.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "alertconfiguration")
public class AlertConfiguration extends AbstractAlertConfiguration {
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "alert_id")
	@Cascade(CascadeType.ALL)
	private UserAlert userAlert;

}
