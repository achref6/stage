package stage.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor


@Getter
@Setter
@Entity
@Table(name = "user_alert_template")
public class UserAlertTemplate extends AbstractUserAlert {
	
	@Cascade(CascadeType.ALL)
	@ManyToMany(mappedBy = "userAlert")
	private Set<AlertConfigurationTemplate> alertconfigurations;

}
