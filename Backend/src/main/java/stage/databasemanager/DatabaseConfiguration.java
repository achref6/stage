package stage.databasemanager;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatabaseConfiguration {

    @Autowired
    private DataSourceManager dataSourceManager;
    @Bean
    public DataSource clientDatasource() {
        return dataSourceManager.getDataSourceRouter();
    }
    

}
