package stage.databasemanager;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Service;

@Service
public class DatabaseService {
	
	@Autowired
	private DataSourceManager dataSourceManager;
	
	public void addDatabase(String name) {
		DataSource dataSource =new DriverManagerDataSource("jdbc:mysql://localhost:3306/", "root", "password");
		try {
			dataSource.getConnection().createStatement().execute("create database `"+name+"`;");
			dataSourceManager.addDataSource(name, new DriverManagerDataSource("jdbc:mysql://localhost:3306/"+name, "root", "password"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
