package stage.databasemanager;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class DataSourceManager  {
	private Map<Object, Object> datasources;    
    private DataSourceRouter dataSourceRouter;


	public DataSourceManager(){
		datasources = new HashMap<Object, Object>();
		datasources.put(null,new DriverManagerDataSource("jdbc:mysql://localhost:3306/stage0", "root", "password"));
		datasources.put("stage0",new DriverManagerDataSource("jdbc:mysql://localhost:3306/stage0", "root", "password"));
		datasources.put("stage1",new DriverManagerDataSource("jdbc:mysql://localhost:3306/stage1", "root", "password"));
		dataSourceRouter = new DataSourceRouter();
		dataSourceRouter.setTargetDataSources(datasources);
		dataSourceRouter.afterPropertiesSet();
	}
	public void addDataSource(String dbname,DataSource dataSource) {
		datasources.put(dbname, dataSource);
		dataSourceRouter.afterPropertiesSet();
	}
	public DataSource getDataSourceRouter() {
		return dataSourceRouter;
	}

}
