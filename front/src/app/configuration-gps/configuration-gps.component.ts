import { Component, Input, OnInit } from '@angular/core';
import { AlertConfiguration } from '../models/AlertConfiguration';
import { Operator } from '../models/Operator';

@Component({
  selector: 'app-configuration-gps',
  templateUrl: './configuration-gps.component.html',
  styleUrls: ['./configuration-gps.component.css'],
})
export class ConfigurationGpsComponent implements OnInit {
  @Input()
  public alertConfiguration :AlertConfiguration;
  public point = { x: '0', y: '0' };

  constructor() {}

  ngOnInit() {
    this.Init();
  }
  public update(): void {
    this.alertConfiguration.value1 = this.point.x + ';' + this.point.y;
  }
  public Init(): void {
    if (this.alertConfiguration.value1 != '') {
      let p = this.alertConfiguration.value1.split(';');
      this.point.x=p[0];
      this.point.y=p[1];
      console.log(this.point);
    }
  }
  compareOperator(op1:Operator,op2:Operator){
    return op1.id==op2.id;
  }

}
