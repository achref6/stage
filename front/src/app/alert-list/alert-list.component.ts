import { VehiculeService } from './../services/vehicule.service';
import { MesureService } from './../services/mesure.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { Alert } from '../models/Alert';
import { AlertService } from '../services/alert.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { Mesure } from '../models/Mesure';
import { AlertConfiguration } from '../models/AlertConfiguration';
import { AlertTemplateService } from '../services/alert-template.service';
import { Vehicule } from '../models/Vehicule';

@Component({
  selector: 'app-alert-list',
  templateUrl: './alert-list.component.html',
  styleUrls: ['./alert-list.component.css'],
})
export class AlertListComponent implements OnInit {
  alerts: Alert[];
  displayAlerts: Alert[];
  alert: Alert = {
    id: null,
    name: null,
    frequency: null,
    timeToRealert: null,
    active: null,
    emailMode: null,
    email: null,
    alertconfigurations: null,
    vehicule: null,
    groupe: null,
  };
  alertConfigurations: AlertConfiguration[] = [];

  loadingIndicator = false;
  reorderable = true;

  columns = [
    { prop: 'name' },
    { name: 'groupe' },
    { name: 'vehicule' },
    { name: 'frequency' },
    { name: 'timeToRealert' },
    { name: 'active' },
    { name: 'emailMode' },
    { name: 'email' },
    { name: 'actions' },
  ];

  ColumnMode = ColumnMode;
  mesures: Mesure[];
  selectedmesures= [];
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 10,
    allowSearchFilter: true,
  };
  templates : Alert[] =[];
  selectedTemplate : Alert;
  vehicules: Vehicule[];

  constructor(
    private alertService: AlertService,
    private alertTemplateService: AlertTemplateService,
    private router: Router,
    private mesureService: MesureService,
    private vehiculeService: VehiculeService
  ) {}

  ngOnInit() {
    this.getAlerts();
    this.getAlertTemplates();
    this.mesureService.getMesures().subscribe(
      (response: Mesure[]) => {
        this.mesures = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );

    this.vehiculeService.getVehicules().subscribe(
      (response: Vehicule[]) => {
        this.vehicules = response;
        this.alert.vehicule=this.vehicules[0];
        this.alert.groupe=this.alert.vehicule.groupes[0];
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  deleteTemplate(){
    this.alertTemplateService.deleteAlert(this.selectedTemplate.id).subscribe(
      (reponse) =>{
        this.getAlertTemplates()
      },
      (error:HttpErrorResponse) =>{
        console.log(error);
      }
    )
    this.selectedTemplate=this.templates[0];

  }
  addConfiguration(mes): void {
    let alertConfiguration: AlertConfiguration = {
      id: null,
      operator: mes.operators[0],
      value1: '',
      value2: '',
      mesure: mes,
      active: false,
      userAlert: null,
    };
    this.alertConfigurations.push(alertConfiguration);
  }
  getConfiguration(mesid): AlertConfiguration {
    return this.alertConfigurations.find((config) => {
      return config.mesure.id == mesid;
    });
  }
  public createAlert(): void {
    this.alert = {
      id: null,
      name: null,
      frequency: null,
      timeToRealert: null,
      active: null,
      emailMode: null,
      email: null,
      alertconfigurations: null,
      vehicule: this.vehicules[0],
      groupe: this.vehicules[0].groupes[0],
    };
    this.alertConfigurations = [];
    this.mesures.forEach((mes) => this.addConfiguration(mes));
    this.selectedmesures=[];
  }
  public setMail(value) {
    this.alert.email = value.emails;
    this.alert.emailMode = value.emailMode;
  }

  public goToAlert(alertId: Number) {
    this.router.navigateByUrl(`alert/${alertId}`);
  }
  public searchAlert(alertName: string) {
    this.displayAlerts = this.alerts.filter((alert) =>
      alert.name.includes(alertName)
    );
  }
  public getAlertTemplates(): void {
    this.alertTemplateService.getAlerts().subscribe(
      (response: Alert[]) => {
        this.templates = response;
        this.selectedTemplate=this.templates[0];
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getAlerts(): void {
    this.alertService.getAlerts().subscribe(
      (response: Alert[]) => {
        this.alerts = response;
        this.displayAlerts = Object.assign([], this.alerts);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  public delete(alert): void {
    this.alertService.deleteAlert(alert.id).subscribe((response) => {
      console.log(response);
      this.getAlerts();
    });
  }
  public loadTemplate():void{
    this.alert = JSON.parse(JSON.stringify(this.selectedTemplate));
    this.alertConfigurations = this.alert.alertconfigurations;
    this.selectedmesures=[];
    this.alert.vehicule=this.vehicules[0];
    this.alert.groupe=this.vehicules[0].groupes[0];
    this.alertConfigurations.forEach((config)=>{
      if(config.active){
        this.selectedmesures.push(config.mesure)
      }
    })
  }
  public compareID(o1,o2){
    return o1.id==o2.id;
  }
  public initGroupe(){
    this.alert.groupe=this.alert.vehicule.groupes[0];
  }
  public edit(row): void {
    this.alert = JSON.parse(JSON.stringify(row));
    this.alertConfigurations = this.alert.alertconfigurations;
    this.selectedmesures=[];
    this.alertConfigurations.forEach((config)=>{
      if(config.active){
        this.selectedmesures.push(config.mesure)
      }
    })
  }
  public saveTemplate() {
    this.alert.alertconfigurations = this.alertConfigurations;
    this.alertConfigurations.forEach(
      (config) => (config.active = this.isSelected(config.mesure.id))
    );
    let alert = JSON.parse(JSON.stringify(this.alert));
    alert.id=null;
    this.alertTemplateService.addAlert(alert).subscribe(
      (response) => {
        this.getAlertTemplates();
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    );
  }
  public save() {
    this.alert.alertconfigurations = this.alertConfigurations;
    this.alertConfigurations.forEach(
      (config) => (config.active = this.isSelected(config.mesure.id))
    );
    console.log(this.alert)
    this.alertService.addAlert(this.alert).subscribe(
      (response) => {
        this.getAlerts();
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    );
  }
  private isSelected(mesureid) {
    return this.selectedmesures.findIndex((mes) => mes.id == mesureid) != -1;
  }
}
