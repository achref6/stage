import { Operator } from './Operator';
export interface Mesure {
  id: number;
	name: string;
  unit: string;
  operators:Operator[];
}
