import { Alert } from './Alert';
import { Mesure } from "./Mesure";
import { Operator } from "./Operator";

export interface AlertConfiguration {
  id: number;
	operator: Operator;
	value1: string;
	value2: string;
	mesure: Mesure;
	active: boolean;
  userAlert: Alert;
}
