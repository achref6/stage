import { AlertConfiguration } from './AlertConfiguration';
import { Groupe } from './Groupe';
import { Vehicule } from './Vehicule';
export interface Alert {
	id: number;
	name: string;
	frequency: string;
	timeToRealert: number;
	active: boolean;
	emailMode: boolean;
	email: string;
  alertconfigurations: AlertConfiguration[];
	vehicule: Vehicule;
	groupe: Groupe;
}
