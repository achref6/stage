import { Groupe } from "./Groupe";

export interface Vehicule {
  id: number;
	alias: string;
  groupes : Groupe[];
}
