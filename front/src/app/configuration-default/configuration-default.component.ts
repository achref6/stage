import { Operator } from './../models/Operator';
import { Component, Input, OnInit } from '@angular/core';
import { AlertConfiguration } from '../models/AlertConfiguration';

@Component({
  selector: 'app-configuration-default',
  templateUrl: './configuration-default.component.html',
  styleUrls: ['./configuration-default.component.css']
})
export class ConfigurationDefaultComponent implements OnInit {

  @Input()
  public alertConfiguration :AlertConfiguration;



  constructor(){
  }

  ngOnInit() {
    this.Init();
  }
  public Init(): void {
  }
  public unary(): boolean{
    if(this.alertConfiguration.operator==null)
      return false;
    if(this.alertConfiguration.operator.name=="between"){
      return false;
    }
    this.alertConfiguration.value2="";
    return true;
  }
  compareOperator(op1:Operator,op2:Operator){
    return op1.id==op2.id;
  }



}
