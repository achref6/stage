import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Alert } from '../models/Alert';
import { Header } from '../header';

@Injectable({providedIn: 'root'})
export class AlertService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient){}

  public getAlerts(): Observable<Alert[]> {
    return this.http.get<Alert[]>(`${this.apiServerUrl}/useralert`,Header.httpOptions);
  }
  public getAlert(id: Number): Observable<Alert> {
    return this.http.get<Alert>(`${this.apiServerUrl}/useralert/${id}`,Header.httpOptions);
  }


  public addAlert(alert: Alert): Observable<Alert> {
    return this.http.post<Alert>(`${this.apiServerUrl}/useralert`, alert,Header.httpOptions);
  }

  public updateAlert(alert: Alert): Observable<Alert> {
    return this.http.put<Alert>(`${this.apiServerUrl}/useralert`, alert,Header.httpOptions);
  }

  public deleteAlert(alertId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/useralert/${alertId}`,Header.httpOptions);
  }
}
