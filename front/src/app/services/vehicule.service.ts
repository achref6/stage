import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Header } from '../header';
import { Vehicule } from '../models/Vehicule';

@Injectable({
  providedIn: 'root'
})
export class VehiculeService {

  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient){}

  public getVehicules(): Observable<Vehicule[]> {
    return this.http.get<Vehicule[]>(`${this.apiServerUrl}/vehicule`,Header.httpOptions);
  }

}
