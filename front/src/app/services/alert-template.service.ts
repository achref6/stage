import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Header } from '../header';
import { Alert } from '../models/Alert';

@Injectable({
  providedIn: 'root'
})
export class AlertTemplateService {

  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient){}

  public getAlerts(): Observable<Alert[]> {
    return this.http.get<Alert[]>(`${this.apiServerUrl}/useralerttemplate`,Header.httpOptions);
  }
  public getAlert(id: Number): Observable<Alert> {
    return this.http.get<Alert>(`${this.apiServerUrl}/useralerttemplate/${id}`,Header.httpOptions);
  }


  public addAlert(alert: Alert): Observable<Alert> {
    return this.http.post<Alert>(`${this.apiServerUrl}/useralerttemplate`, alert,Header.httpOptions);
  }

  public updateAlert(alert: Alert): Observable<Alert> {
    return this.http.put<Alert>(`${this.apiServerUrl}/useralerttemplate`, alert,Header.httpOptions);
  }

  public deleteAlert(alertId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/useralerttemplate/${alertId}`,Header.httpOptions);
  }

}
