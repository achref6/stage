/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AlertConfigurationService } from './alert-configuration.service';

describe('Service: AlertConfiguration', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AlertConfigurationService]
    });
  });

  it('should ...', inject([AlertConfigurationService], (service: AlertConfigurationService) => {
    expect(service).toBeTruthy();
  }));
});
