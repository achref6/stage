import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Header } from '../header';
import { AlertConfiguration } from '../models/AlertConfiguration';

@Injectable({
  providedIn: 'root'
})
export class AlertConfigurationService {

  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient){}

  public getAlertConfigurations(): Observable<AlertConfiguration[]> {
    return this.http.get<AlertConfiguration[]>(`${this.apiServerUrl}/alertconfiguration`,Header.httpOptions);
  }
  public getAlertConfiguration(alertConfigurationId: Number): Observable<AlertConfiguration> {
    return this.http.get<AlertConfiguration>(`${this.apiServerUrl}/alertconfiguration/${alertConfigurationId}`,Header.httpOptions);
  }
  public addAlertConfiguration(alertConfiguration: AlertConfiguration): Observable<AlertConfiguration> {
    return this.http.post<AlertConfiguration>(`${this.apiServerUrl}/alertconfiguration`, alertConfiguration,Header.httpOptions);
  }

  public updateAlertConfiguration(alertConfiguration: AlertConfiguration): Observable<AlertConfiguration> {
    return this.http.put<AlertConfiguration>(`${this.apiServerUrl}/alertconfiguration`, alertConfiguration,Header.httpOptions);
  }

  public deleteAlertConfiguration(alertConfigurationId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/alertconfiguration/${alertConfigurationId}`,Header.httpOptions);
  }

}
