import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Mesure } from '../models/Mesure';
import { Header } from '../header';

@Injectable({
  providedIn: 'root'
})
export class MesureService {

  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient){}

  public getMesures(): Observable<Mesure[]> {
    return this.http.get<Mesure[]>(`${this.apiServerUrl}/mesure`,Header.httpOptions);
  }
}
