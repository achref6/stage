/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AlertTemplateService } from './alert-template.service';

describe('Service: AlertTemplate', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AlertTemplateService]
    });
  });

  it('should ...', inject([AlertTemplateService], (service: AlertTemplateService) => {
    expect(service).toBeTruthy();
  }));
});
