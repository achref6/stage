/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MesureService } from './mesure.service';

describe('Service: Mesure', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MesureService]
    });
  });

  it('should ...', inject([MesureService], (service: MesureService) => {
    expect(service).toBeTruthy();
  }));
});
