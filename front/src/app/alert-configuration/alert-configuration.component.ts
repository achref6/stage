import { Mesure } from './../models/Mesure';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AlertConfiguration } from '../models/AlertConfiguration';

@Component({
  selector: 'app-alert-configuration',
  templateUrl: './alert-configuration.component.html',
  styleUrls: ['./alert-configuration.component.css']
})
export class AlertConfigurationComponent implements OnInit {

  constructor() { }
  @Input()
  alertConfiguration : AlertConfiguration ;

  @Output() onChange: EventEmitter<any> = new EventEmitter();



  ngOnInit() {
  }

}
