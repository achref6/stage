import { HttpHeaders } from '@angular/common/http';

export class Header {
  static httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Database': "stage0",
    }),
  };
}
