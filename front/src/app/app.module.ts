import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AlertService } from './services/alert.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfigurationDefaultComponent } from './configuration-default/configuration-default.component';
import { ConfigurationGpsComponent } from './configuration-gps/configuration-gps.component';
import { SetDatabaseComponent } from './set-database/set-database.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { EmailManagerComponent } from './email-manager/email-manager.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AlertConfigurationComponent } from './alert-configuration/alert-configuration.component';
import { ConfigurationDateComponent } from './configuration-date/configuration-date.component';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    ConfigurationDefaultComponent,
    ConfigurationGpsComponent,
    SetDatabaseComponent,
    EmailManagerComponent,
      AlertConfigurationComponent,
      ConfigurationDateComponent
   ],
  imports: [
    NgMultiSelectDropDownModule,
    MatSlideToggleModule,
    NgxDatatableModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
  ],
  providers: [AlertService],
  bootstrap: [AppComponent],
})
export class AppModule {}
