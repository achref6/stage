import { SetDatabaseComponent } from './set-database/set-database.component';
import { ConfigurationDefaultComponent } from './configuration-default/configuration-default.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlertListComponent } from './alert-list/alert-list.component';

const routes: Routes = [
  { path: '', component: AlertListComponent },
  { path: 'd', component: SetDatabaseComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

export const routingComponents = [
  AlertListComponent,
  ConfigurationDefaultComponent,
  SetDatabaseComponent,
];
