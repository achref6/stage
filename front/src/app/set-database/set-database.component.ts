import { Header } from './../header';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-set-database',
  templateUrl: './set-database.component.html',
  styleUrls: ['./set-database.component.css']
})
export class SetDatabaseComponent implements OnInit {

  constructor() { }
  database :string;

  ngOnInit() {
  }
  set():void{
    Header.httpOptions.headers=Header.httpOptions.headers.set("Database",this.database);
  }

}
