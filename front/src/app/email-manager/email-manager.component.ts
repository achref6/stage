import { Component, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-email-manager',
  templateUrl: './email-manager.component.html',
  styleUrls: ['./email-manager.component.css']
})
export class EmailManagerComponent {
  /*@Input()*/
  _emailsHash: string = null;

  @Input() emailMode: boolean = false;

  public emailValue: string = null;

  public actionMode: 'ADD' | 'UPDATE' = 'ADD';

  public emailOldValue = null;

  emails: string[] = [];

  EMAIL_MAX_LENGTH = 10;

  @Output() onChange: EventEmitter<any> = new EventEmitter();

  constructor() {
    if (this.emailsHash) {
      this.emails = this.emailsHash.split(';');
    }
  }

  get emailsHash(): string {
    return this._emailsHash;
  }

  @Input()
  set emailsHash(emailsHash: string) {
    this._emailsHash = emailsHash;
    if (this._emailsHash && this._emailsHash.trim() !== '') {
      this.emails = this._emailsHash.split(';');
    } else {
      this.emails = [];
    }
  }

  addEmail() {
    this.emails.push(this.emailValue);
    this.emailValue = null;
    this.emitValue();
  }

  updateEmail() {
    const index = this.emails.indexOf(this.emailOldValue);

    if (index === -1) {
      this.init();
      return;
    }

    this.emails[index] = this.emailValue;
    this.actionMode = 'ADD';
    this.emailOldValue = null;
    this.emailValue = null;
    this.emitValue();
  }

  deleteEmail(email) {
    let index = this.emails.indexOf(email);
    if (index > -1) {
      this.emails.splice(index, 1);
    }
    this.emitValue();
  }

  emailToHash(emails: string[]) {
    let separator = '';
    let hash = '';
    for (let i = 0; i < emails.length; i++) {
      hash += separator + emails[i];
      separator = ';';
    }
    return hash;
  }

  emitValue() {
    this.onChange.emit({
      emailMode: this.emailMode,
      emails: this.emailToHash(this.emails)
    });
  }

  init() {
    this.actionMode = 'ADD';
    this.emailOldValue = null;
    this.emailValue = null;
  }
}
