import { Operator } from './../models/Operator';
import { Component, Input, OnInit } from '@angular/core';
import { AlertConfiguration } from '../models/AlertConfiguration';

@Component({
  selector: 'app-configuration-date',
  templateUrl: './configuration-date.component.html',
  styleUrls: ['./configuration-date.component.css']
})
export class ConfigurationDateComponent implements OnInit {

  @Input()
  public alertConfiguration :AlertConfiguration;



  constructor(){
  }
  ngOnInit() {
  }
  compareOperator(op1:Operator,op2:Operator){
    return op1.id==op2.id;
  }
}
